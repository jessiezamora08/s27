const http = require ('http');


let directory = [
    {
        "name":"Thoma", 
        "email": "malewife@gmail.com"
    },
    {
        "name":"Tartaglia", 
        "email": "childe@gmail.com" 
    }
]
//  GET ALL users
const server = http.createServer((req,res) =>{
    if(req.url == "/users" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'})
        res.write(JSON.stringify(directory))
        res.end();
    }

    // ADD NEW user
    if(req.url == "/users" && req.method == "POST"){
        // res.writeHead(200,{'Content-Type':'text/plain'})
        // res.end('Data to be sent to the database');
    

        let requestBody = '';

        req.on('data', (data) => {
            requestBody += data; 
        });

        req.on ('end', () => {
            requestBody = JSON.parse(requestBody);
            console.log(requestBody);


            let newUser = {
                "name" : requestBody.name, 
                "email" :requestBody.email
            }

            directory.push(newUser)
            console.log(directory)

            res.writeHead(200, {'Content-type': 'application/json'});
            res.write(JSON.stringify(newUser))
            res.end ();
        })
    }
})

server.listen(4000,'localhost', () => {
    console.log('listening to port 4000')
})